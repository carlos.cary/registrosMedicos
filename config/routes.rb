Rails.application.routes.draw do
  resources :comments
  resources :analyses
  resources :static_pages
  root 'static_pages#index'
  resources :consultations  do
    get "allindex", on: :collection
  end
  get 'micontroller/allindex'
  devise_for :patients do
  end
  get "/static_pages/index" => "static_pages#index"
  #PATIENT INFORMATION
  match '/patient', to: 'patients#show', via: 'get'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end