class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.date :date
      t.string :reason
      t.text :comment
      t.references :patient, foreign_key: true

      t.timestamps
    end
  end
end
