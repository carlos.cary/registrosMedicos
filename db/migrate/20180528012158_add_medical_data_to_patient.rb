class AddMedicalDataToPatient < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :bloodType, :string
    add_column :patients, :importantDiseases, :text
    add_column :patients, :allergies, :text
  end
end
