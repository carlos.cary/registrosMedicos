class AddImageToConsultations < ActiveRecord::Migration[5.2]
  def change
    add_column :consultations, :consultationPicture, :string
  end
end
