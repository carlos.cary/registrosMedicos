class CreateConsultations < ActiveRecord::Migration[5.2]
  def change
    create_table :consultations do |t|
      t.date :date
      t.text :reason
      t.string :doctor
      t.string :specialty
      t.text :prescription
      t.text :observations
      t.references :patient, foreign_key: true
      t.timestamps
    end
  end
end
