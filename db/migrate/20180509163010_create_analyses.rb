class CreateAnalyses < ActiveRecord::Migration[5.2]
  def change
    create_table :analyses do |t|
      t.string :reason
      t.date :date
      t.string :laboratory
      t.string :typeOfAnalisys
      t.text :observations
      t.string :analisysPicture
      t.references :patient, foreign_key: true

      t.timestamps
    end
  end
end
