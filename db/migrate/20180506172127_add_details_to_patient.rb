class AddDetailsToPatient < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :profilePicture, :string
    add_column :patients, :name, :string
    add_column :patients, :lastName, :string
  end
end
