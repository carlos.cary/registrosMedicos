class AddColumnsToPatients < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :direction, :string
    add_column :patients, :birthdate, :date
    add_column :patients, :telephone, :integer
  end
end
