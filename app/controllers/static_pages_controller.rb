class StaticPagesController < ApplicationController
    before_action :authenticate_patient! 
    def index
        @consultations = current_patient.consultations.order('created_at DESC')
        @analyses = current_patient.analyses.order('created_at DESC')
        @comments = current_patient.comments.order('created_at DESC')
        respond_to do |format|
            format.html
            format.json
            format.pdf { render template: 'static_pages/all', pdf: 'Historial médico'}
          end
    end
end