class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    def after_sign_out_path_for(resource)
        new_patient_session_path
    end
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:profilePicture, :name, :lastName, :direction, :bloodType, :importantDiseases, :allergies, :birthdate, :telephone

        ])
        devise_parameter_sanitizer.permit(:account_update, keys: [:profilePicture, :name, :lastName, :direction, :bloodType, :importantDiseases, :allergies, :birthdate, :telephone 
        ])
    end
end
