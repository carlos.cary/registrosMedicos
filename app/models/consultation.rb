class Consultation < ApplicationRecord
  validates :doctor, presence: { message: ": El nombre del doctor no puede estar vacío" }
  mount_uploader :consultationPicture, ConsultationimageUploader
  belongs_to :patient
end
