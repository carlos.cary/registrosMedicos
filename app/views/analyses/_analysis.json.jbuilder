json.extract! analysis, :id, :reason, :date, :laboratory, :typeOfAnalisys, :observations, :analisysPicture, :patient_id, :created_at, :updated_at
json.url analysis_url(analysis, format: :json)
