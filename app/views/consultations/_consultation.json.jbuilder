json.extract! consultation, :id, :date, :reason, :doctor, :specialty, :prescription, :observations, :patient_id, :created_at, :updated_at
json.url consultation_url(consultation, format: :json)
