json.extract! comment, :id, :date, :reason, :comment, :patient_id, :created_at, :updated_at
json.url comment_url(comment, format: :json)
